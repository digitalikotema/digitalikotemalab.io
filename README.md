# Digital iko Tema Website

A website made with [VueJs](http://vuejs.org).

Online at: https://ikotema.digital

## Features

- Prerendering at build time for SEO improvement and no JS support
- Content in .yaml files
- Multilanguage (i18n)
- Lazy image load and fullscreen on click support
- Automatic build for Gitlab pages
- Image minification
- Helper scripts for font minification and thumbnail generation

## Content

To change the content edit the files in `data` folder. Then build again.

To add a project, it must be added to the projects map in the .yaml and also to the order array in the same file.

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

### Fonts

Minify fonts:

``` bash
fontmin NunitoSans-Light.ttf build -d
fontmin NunitoSans-ExtraLight.ttf build -d -t 'Digital iko Tema'
```
    
Copy `woff` and `ttf` files in `build` folder to `src/assets/fonts/nunito-sans`.

### Thumbnails

Run `scripts/thumbnails.sh` to generate thumbnails for images in `src/assets/imgs/full`.
