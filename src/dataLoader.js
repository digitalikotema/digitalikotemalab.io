import {getDefaultLocale} from '@/utils.js'

import andres from '@/../data/andres.yaml'
import digital from '@/../data/digital.yaml'
var data = {andres, digital}

// Fills the `target` object with `model` attributes when `target` attributes
// are undefined.
// So if target = {a: 'hello'} and model = {a: 'oi', b: 'ok'},
// target becomes: {a: 'hello', b: 'ok'}
function deepFill (target, model) {
  for (var key in model) {
    // If undefined
    if (typeof target[key] === 'undefined') {
      // If simple type, copy
      if (['string', 'number', 'boolean'].includes(typeof model[key])) {
        target[key] = model[key]
      }
    }

    // If object
    if (typeof model[key] === 'object') {
      if (!target[key]) target[key] = {}
      // recurse inside
      deepFill(target[key], model[key])
      // if (model[key][0]) console.log('Array!', key)
    }
  }
}

// Fills empty values in all other translations based on a main one.
// So if mainTranslationCode = 'pt' and:
//   obj = {
//     pt: {a: 'oi', b: 'ok'},
//     en: {a: 'hi'},
//     es: {a: 'hola'}
//   }
// The obj becomes:
//   {
//     pt: {a: 'oi', b: 'ok'},
//     en: {a: 'hi', b: 'ok'},
//     es: {a: 'hola', b: 'ok'}
//   }
function fillTranslations (obj, mainTranslationCode) {
  for (var code in obj) {
    if (code !== mainTranslationCode) {
      deepFill(obj[code], obj[mainTranslationCode])
    }
  }
  return obj
}

for (var key in data) {
  fillTranslations(data[key], getDefaultLocale())
}

export default data
