import {setMeta} from '@/utils'

// Makes the component set meta attributes and title for the page when it's
// mounted. To update these values when route changes, a `router.afterEach` is
// also needed.
// The component must have a `this.data.meta` with the metadata.
export var metaSetter = {
  computed: {
    meta () { return this.data.meta }
  },
  mounted () {
    if (this.meta) setMeta(this.meta)
  }
}
