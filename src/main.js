import Vue from 'vue'
import VueI18n from 'vue-i18n'
import VueLazyload from 'vue-lazyload'

import App from '@/App'
import router from '@/router'
import {setMeta, getDefaultLocale} from '@/utils.js'
import translations from '@/../data/translations.yaml'

import vueSmoothScroll from 'vue-smooth-scroll'
Vue.use(vueSmoothScroll)

Vue.component('svg-icon', require('@/components/SvgIcon').default)
Vue.component('email-link', require('@/components/EmailLink').default)

Vue.config.productionTip = false

Vue.use(VueLazyload, {
  preLoad: 1.3,
  // error: 'dist/error.png',
  loading: require('@/assets/imgs/full/eclipse.svg'),
  attempt: 1
})

Vue.use(VueI18n)
const i18n = new VueI18n({
  locale: getDefaultLocale(),
  silentTranslationWarn: true,
  messages: translations
})

// Plugins
//   Fullscreen img
var FullscreenImgPlugin = {}
FullscreenImgPlugin.install = function (Vue, options) {
  Vue.prototype.$fullscreenImg = new Vue()
}
Vue.use(FullscreenImgPlugin)

// Handle locale across routing.
// Not really needed if all router-links enforce route lang.
router.beforeResolve((to, from, next) => {
  var toLang = to.params.lang
  var fromLang = from.params.lang
  // Set locale
  if (toLang) i18n.locale = toLang
  // If locale is set, carry it to future routes
  if (!toLang && fromLang) {
    next({name: to.name, params: {lang: fromLang}})
  } else next()
})

router.afterEach((to, from) => {
  // Updates meta attributes after route changes.
  // Doesn't work at the initial route.
  if (router.app.$children.length) {
    setMeta(router.app.$children[0].$refs.main.meta)
  }
})

/* eslint-disable no-new */
new Vue({
  i18n,
  router,
  el: '#app',
  render: h => h(App)
})
