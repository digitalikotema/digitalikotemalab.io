import Vue from 'vue'
import Router from 'vue-router'
import Person from '@/components/PersonPage'
import Home from '@/components/HomePage'
import MenuItems from '@/components/MenuItems'

Vue.use(Router)

export default new Router({
  mode: 'history',
  scrollBehavior (to, from, savedPosition) {
    // If changing component, add a delay to wait transition effect
    var delay = to.name !== from.name ? 1100 : 0
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        if (savedPosition) {
          resolve(savedPosition)
        } else if (to.hash) {
          resolve({
            selector: to.hash
          })
        } else resolve({ x: 0, y: 0 })
      }, delay)
    })
  },
  routes: [
    {
      path: '/:lang?/andres',
      name: 'andres',
      components: {
        default: Person,
        menu: MenuItems
      },
      props: {
        default: true,
        menu: {items: Person.menuItems}
      }
    },
    {
      path: '/:lang?',
      name: 'home',
      component: Home
    }
  ]
})
