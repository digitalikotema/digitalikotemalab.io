import config from '@/../data/config.yaml'

// Load icons into an object to be used by a componet.
// list: icons to be loaded.
// remap: icons to be loaded with a different name
// export function loadIcons (list, remap) {
//   var components = {}
//   for (let name of list) {
//     components[name + '-icon'] = require('vue-feather-icon/components/' + name).default
//   }
//   for (let name in remap) {
//     var newName = remap[name]
//     components[newName + '-icon'] = require('vue-feather-icon/components/' + name).default
//   }
//   return components
// }

export function setMeta (meta) {
  // Set title
  document.title = meta.title
  for (var key in meta) {
    if (key !== 'title') {
      document.querySelector(`meta[name="${key}"]`).content = meta[key]
    }
  }
}

export function setCanonicalLink () {
  // The canonical url is made from the configured domain, and the current
  // pathname striped from the locale if it's the default one.
  var canonical = config.domain
  var path = location.pathname.replace(RegExp(`/${getDefaultLocale()}(/|$)`), '$1')
  if (path !== '/') canonical += path
  document.querySelector(`link[rel="canonical"]`).setAttribute('href', canonical)
}

// export function resolveUrlHelper (path, subpath) {
//   if (path.slice(0, 4) === 'http') return path
//   else return require(`@/assets/${subpath}/${path}`)
// }

export function getImgUrl (img, thumbnail = true) {
  if (img.slice(0, 4) === 'http') return img
  else if (thumbnail) return require('@/assets/imgs/thumbnails/projects/' + img)
  else return require('@/assets/imgs/full/projects/' + img)
}

export function getDefaultLocale () {
  return config.locales[0].code
}

// Set the HTML tag lang attribute
export function setHtmlLocale (code) {
  document.querySelector('html').setAttribute('lang', code)
}
