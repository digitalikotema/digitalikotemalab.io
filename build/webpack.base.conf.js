'use strict'
const path = require('path')
const utils = require('./utils')
const config = require('../config')
const webpack = require('webpack')
const vueLoaderConfig = require('./vue-loader.conf')

// const ThumbnailPlugin = require('thumbnail-webpack-plugin')

function resolve (dir) {
  return path.join(__dirname, '..', dir)
}

module.exports = {
  entry: {
    app: './src/main.js'
  },
  output: {
    path: config.build.assetsRoot,
    filename: '[name].js',
    publicPath: process.env.NODE_ENV === 'production'
      ? config.build.assetsPublicPath
      : config.dev.assetsPublicPath
  },
  resolve: {
    extensions: ['.js', '.vue', '.json'],
    alias: {
      '@': resolve('src'),
    }
  },
  // plugins: [
  //   new webpack.ContextReplacementPlugin(
  //       /vue-feather-icon[\/\\]components$/,
  //       /award|users|gitlab|github|settings|external\-link|\/mail|\/lock|\/book|compass|clipboard|alert\-triangle|menu|globe|home/
  //   )
  // ],
  module: {
    rules: [
      {
        test: /\.(js|vue)$/,
        loader: 'eslint-loader',
        enforce: 'pre',
        include: [resolve('src'), resolve('test')],
        options: {
          formatter: require('eslint-friendly-formatter')
        }
      },
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        options: vueLoaderConfig
      },
      {
        test: /\.js$/,
        loader: 'babel-loader',
        include: [resolve('src'), resolve('test')]
      },
      {
        test: /\.yaml$/,
        loader: 'json-loader!yaml-loader',
        include: [resolve('data')]
      },
      {
        test: /\.(asc)(\?.*)?$/,
        loader: 'url-loader',
        options: {
          limit: 1,
          name: utils.assetsPath('texts/[name].[hash:7].[ext]')
        }
      },
      {
        test: /\.svg$/,
        use: [{
          loader: 'raw-loader',
        }, {
          loader: 'image-webpack-loader',
          options: {
            bypassOnDebug: false,
            svgo: {
              plugins: [
                {precision: 2},
				        {removeXMLNS: true},
                {removeDimensions: true},
                {convertShapeToPath: false},
                {removeAttrs: { attrs: ['class', 'fill', 'stroke.*'] }},
			        ]
            }
          }
        }],
        include: [resolve('src/icons')]
      },
      {
        test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
        include: [resolve('src/assets/imgs')],
        use: [{
            loader: 'url-loader',
            options: {
              limit: 1,
              name: utils.assetsPath('img/[name].[hash:7].[ext]')
            },
          }, {
            loader: 'image-webpack-loader',
            options: {
              bypassOnDebug: true,
              mozjpeg: {
                progressive: true,
                quality: 85
              },
              // optipng.enabled: false will disable optipng
              optipng: {
                enabled: false,
              },
              pngquant: {
                quality: '75-90',
                speed: 4
              },
              gifsicle: {
                interlaced: false,
              }
            }
          }],
      },
      {
        test: /\.(mp4|webm|ogg|mp3|wav|flac|aac)(\?.*)?$/,
        loader: 'url-loader',
        options: {
          limit: 10000,
          name: utils.assetsPath('media/[name].[hash:7].[ext]')
        }
      },
      {
        test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
        loader: 'url-loader',
        options: {
          limit: 10000,
          name: utils.assetsPath('fonts/[name].[hash:7].[ext]')
        }
      }
    ]
  }
}
