#!/bin/env python

'''
Downloads images from an .yaml file.

Renames the downloaed images based on yaml object ids.
'''

import os
from ruamel.yaml import YAML
from urllib.request import urlretrieve

yaml_file = '../data/andres.yaml'
imgs_folder = 'imgs'
output = 'andres2.yaml'
maps = ['projects', 'events', 'experiments']
os.makedirs(imgs_folder, exist_ok=True)

yaml = YAML()
yaml.width = 400000


with open(yaml_file, 'r') as f:
    data = yaml.load(f)

for category in maps:
    for id_, item in data['pt'][category].items():
        url = item.get('img')
        if url and url.startswith('http'):
            print(id_, url)
            extension = url.rpartition('.')[2]
            if extension not in ['png', 'svg', 'jpg', 'jpeg']:
                print('> Unknown extension:', extension)
                extension = ''
            filename = id_ + '.' + extension
            urlretrieve(url, os.path.join(imgs_folder, filename))
            item['img'] = filename

with open(output, 'w') as f:
    yaml.dump(data, f)
