#!/bin/env bash

cd ../src/assets/imgs
# Get fresh full size images
rsync --recursive --delete full/ thumbnails/ --progress
# Resize
for f in thumbnails/* thumbnails/**/*; do
    if [ -f "$f" ]; then
        echo $f
        mogrify -resize 400 $f
    fi
done
